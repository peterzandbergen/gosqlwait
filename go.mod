module gosqlwait

go 1.14

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20200620013148-b91950f658ec
	github.com/spf13/pflag v1.0.5
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
)
