package gosqlwait

import (
	"context"
)

type client struct {
	hallo string
}

// Connect tries to connect to the database using the connection string
//
// Once connected it executes the query. It returns a nil error if successful.
// The non nil error contains a description of the reason why the connect failed.
// The failure is always due to a timeout, but it contains the error of the last attempt.
func Connect(ctx context.Context, connectionString string, query string) error {
	return nil
}

func (c *client) Connect(ctx context.Context) error {
	return nil
}

func (c *client) Test(ctx context.Context) error {
	return nil
}
