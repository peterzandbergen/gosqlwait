package main

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	flag "github.com/spf13/pflag"

	_ "github.com/denisenkom/go-mssqldb"
)

type config struct {
	dbType   string
	server   string
	instance string
	db       string
	username string
	password string
	dbParams map[string]string
	query    string
	timeout  time.Duration
	retry    time.Duration
}

func buildConnectionString(cfg *config) (string, error) {
	var u url.URL
	parts := strings.Split(cfg.server, "/")
	if len(parts) < 1 {
		return "", errors.New("server missing")
	}
	u.Host = parts[0]
	if len(parts) == 2 {
		u.Path = parts[1]
	}
	u.Scheme = cfg.dbType
	u.User = url.UserPassword(cfg.username, cfg.password)
	u.ForceQuery = len(cfg.dbParams) > 0
	vals := &url.Values{}
	if cfg.db != "" {
		vals.Add("database", cfg.db)
	}
	for k, v := range cfg.dbParams {
		vals.Add(k, v)
	}
	u.RawQuery = vals.Encode()
	return u.String(), nil
}

func parseParams(params string) (map[string]string, error) {
	res := make(map[string]string)
	if len(params) == 0 {
		return res, nil
	}
	p := strings.Split(params, ",")
	for _, kvs := range p {
		kv := strings.Split(kvs, "=")
		if len(kv) != 2 {
			return nil, errors.New("error in params")
		}
		res[kv[0]] = kv[1]
	}
	return res, nil
}

func parseConfig(args ...string) (*config, error) {
	cfg := &config{}
	fs := flag.NewFlagSet("default", flag.ContinueOnError)

	fs.StringVarP(&cfg.dbType, "dbtype", "t", "sqlserver", "name and url schema of the database type")
	fs.StringVarP(&cfg.server, "server", "H", "localhost", "database server name with additional port and instance, e.g. localhost:5432/instance")
	fs.StringVarP(&cfg.db, "database", "d", "", "name of the database to connect to, mandatory")
	fs.StringVarP(&cfg.username, "username", "U", "", "username")
	fs.StringVarP(&cfg.password, "password", "P", "", "password")
	fs.StringVarP(&cfg.query, "query", "q", "", "query to run against the database to test if it is ready")

	var timeout, retry int64
	fs.Int64VarP(&timeout, "timeout", "T", 300, "timeout in seconds")
	fs.Int64VarP(&retry, "retry", "R", 5, "time to wait before retry")

	var params string

	var err error
	if len(args) == 0 {
		err = fs.Parse(os.Args[1:])
	} else {
		err = fs.Parse(args)
	}
	if err != nil {
		fs.PrintDefaults()
		return nil, err
	}

	cfg.timeout = time.Duration(timeout) * time.Second
	cfg.retry = time.Duration(retry) * time.Second
	p, err := parseParams(params)
	if err != nil {
		return nil, err
	}
	cfg.dbParams = p

	return cfg, nil
}

func connectDb(cfg *config) (*sql.DB, error) {
	cs, err := buildConnectionString(cfg)
	if err != nil {
		return nil, err
	}
	db, err := sql.Open(cfg.dbType, cs)
	if err != nil {
		return nil, err
	}
	return db, nil

}

func runTest(ctx context.Context, cfg *config) error {
	// Connect to the database with timeout.
	db, err := connectDb(cfg)
	if err != nil {
		return err
	}
	// Run test query.
	_, err = db.ExecContext(ctx, cfg.query)
	if err != nil {
		return err
	}
	return nil
}

func runTestRetry(ctx context.Context, cfg *config) error {
	for {
		err := runTest(ctx, cfg)
		if err == nil {
			return nil
		}
		log.Printf("test failed: %s", err)

		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(cfg.retry):
		
		}
	}
}

// dbError for reporting unsuccesful test
type dbError struct {
	exitCode int
	msg      string
	err 	error
}

func newDbError(msg string, err error, exitCode int) *dbError {
	return &dbError{
		msg:      msg,
		exitCode: exitCode,
		err: err,
	}
}

func (e *dbError) Error() string {
	return e.msg 
}

func (e *dbError) Unwrap() error {
	return e.err	
}

func (e *dbError) ExitCode() int {
	return e.exitCode
}

func main() {
	// Parse configuration
	cfg, err := parseConfig(os.Args[1:]...)
	if err != nil {
		log.Printf("command line error: %s", err.Error())
		os.Exit(4)
	}
	// Start the test
	ctx, cancel := context.WithTimeout(context.Background(), cfg.timeout)
	defer cancel()
	err = runTestRetry(ctx, cfg)
	if err != nil {
		log.Printf("error: %s", err.Error())
		var dberr = &dbError{}
		if errors.As(err, &dberr) {
			os.Exit(dberr.ExitCode())
		}
		os.Exit(1)
	}
	// Log the result
	log.Println("succes")
	// Return the exit code.
	os.Exit(0)
}
