package main

import (
	"testing"
	"time"

	"context"
)

func TestUrl(t *testing.T) {
	cfg := &config{
		dbType:   "sqlserver",
		username: "sa",
		password: "password",
		server:   "localhost:1433",
		dbParams: map[string]string{
			"key1": "value1",
			"key2": "value2",
		},
	}
	cs, err := buildConnectionString(cfg)
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	t.Logf("%s", cs)
	t.Logf("%#v", cfg)
}

func TestParse(t *testing.T) {
	args := []string{
		"command",
		"--server=localhost:1433",
		"--dbtype=sqlserver",
		"--params=key1=val1,key2=val2",
	}
	cfg, err := parseConfig(args...)
	if err != nil {
		t.Fatalf("error parsing config: %s", err)
	}
	if cfg.server != "localhost:1433" {
		t.Errorf("expected %s, got %s", "localhost:1433", cfg.server)
	}
	if cfg.dbType != "sqlserver" {
		t.Errorf("expected %s, got %s", "sqlserver", cfg.dbType)
	}
	if len(cfg.dbParams) != 2 {
		t.Fatalf("params has incorrect number of elements: %d", len(cfg.dbParams))
	}
	if cfg.retry != time.Duration(5)*time.Second {
		t.Errorf("retry error: %d", cfg.retry)
	}

	t.Logf("cfg: %#v", cfg)
}

// Run this test with a running sql server on port 11433
// docker run --rm -p 11433:1433 --env MSSQL_SERVER=db --env MSSQL_PID=Developer --env ACCEPT_EULA=Y --env MSSQL_USER=sa --env SA_PASSWORD=P@ssw0rd  mcr.microsoft.com/mssql/server:2017-latest-ubuntu
func TestOpenDb(t *testing.T) {
	cfg := &config{
		dbType:   "sqlserver",
		username: "sa",
		password: "password",
		server:   "localhost:11433",
	}
	db, err := connectDb(cfg)
	if err != nil {
		t.Fatalf("%s", err)
	}
	t.Logf("%#v", db)
}

func TestRunTest(t *testing.T) {
	cfg := &config{
		dbType:   "sqlserver",
		db:       "master",
		username: "sa",
		password: "P@ssw0rd",
		server:   "localhost:11433",
		query:    "select name from sys.tables",
		timeout:  60,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// ctx := context.Background()
	err := runTest(ctx, cfg)
	if err != nil {
		t.Fatalf("%s", err)
	}
}

func TestRunTestRetry(t *testing.T) {
	cfg := &config{
		dbType:   "sqlserver",
		db:       "master",
		username: "sa",
		password: "P@ssw0rd",
		server:   "localhost:11433",
		query:    "select name from sys.tables",
		timeout:  60 * time.Second,
		retry:    5 * time.Second,
	}
	ctx, cancel := context.WithTimeout(context.Background(), cfg.timeout)
	defer cancel()
	// ctx := context.Background()
	err := runTestRetry(ctx, cfg)
	if err != nil {
		t.Fatalf("%s", err)
	}
}
