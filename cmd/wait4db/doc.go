package main

/*
wait4db tries to connect to a database on a given server

parameters
-server
The name of the server

-db
the name of the database

-username
-password

-query
the query to execute against the database to check its presence

-timeout
timeout in seconds on the total try, defaults to 5 minutes

-interval
interval between tries in seconds. defaults to 5 seconds


Logs to stdout and returns the result as an exit code:
- 0 OK server and database exist
- 1 could not connect to the server within the timeout
- 2 couild not connect to the database within the timeout
- 3 could not successfully execute the query within the timeout
- 4 bad commandline parameter
*/

