FROM golang:latest AS builder
RUN mkdir /work 
ADD . /work/ 
WORKDIR /work/cmd/wait4db
RUN go mod tidy && go mod download
ARG GIT_COMMIT=""
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-X main.GitCommit=${GIT_COMMIT}" -a -installsuffix cgo -o wait4db .
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM scratch
COPY --from=builder /work/cmd/wait4db/wait4db /wait4db
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["/wait4db"]
